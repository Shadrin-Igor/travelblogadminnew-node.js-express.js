var express = require('express');
var router = express.Router();
var passport = require('passport');
var Collors = require('../../models/Collors');
var moduleKey = 'collors';

router.get('/' + moduleKey , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    Collors.find(function (err, data) {
        if (err)res.send({error: err});
            else res.send({data: data});
    })
});

module.exports = router;