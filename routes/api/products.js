var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    queue = require('queue'),
    relationHelper = require("../../libs/relationHelper");

var Marks = require("../../models/Marks"),
    Collors = require("../../models/Collors"),
    Countries = require("../../models/Countries");

var moduleKey = 'products';

router.get('/' + moduleKey + '/:category', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.category;
    if (cat) {

        var modelName = relationHelper.getProductClass(cat);
        var model = require('../../models/' + modelName);
        model.find(function (err, data) {

            loadRelations(data)
                .then(function (items) {

                    //console.log("items", items );
                    if (err)res.send({status: 'error', error: err});
                    else res.send({status: 'ok', data: items});

                }, function (err) {
                    res.send({status: 'error', error: err});
                })

        })

    }
    else res.send({data: '', error: 'no category'});

});

function loadRelations(models) {
    var marksList = {};
    var collorsList = {};
    var countryList = {};

    return new Promise(function (succeed, fail) {
        var q = queue({concurrency: 1});

        for (var i = 0; i < models.length; i++) {

            (function () {

                var n = i;

                // Подгружаем марки
                q.push(function (next) {

                    if (models[n].mark && !marksList[models[n].mark]) {

                        console.log('mark 1', models[n].mark);
                        Marks.findById(models[n].mark)
                            .then(function (item) {
                                if (item) {
                                    console.log('mark 2-1', item, models[n].mark, marksList);
                                    marksList[models[n].mark] = item.name;
                                    console.log('mark 2-2', item, models[n].mark);
                                    models[n].mark = item.name;
                                }

                                console.log('mark 2');
                                next();
                            }, function (err) {
                                console.log('mark 3');
                                console.log('Error', models[n].mark, item.name);
                                next();
                            })

                    } else {
                        if (models[n].mark)models[n].mark = marksList[models[n].mark];
                        next();
                    }
                });
                // Подгружаем цвета
                q.push(function (next) {

                    if (models[n].color && !collorsList[models[n].color]) {

                        Collors.findById(models[n].color)
                            .then(function (item) {

                                if(item){
                                    collorsList[models[n].color] = item.name;
                                    models[n].color = item.name;

                                }

                                next();
                            }, function (err) {
                                console.log('Error', models[n].color, item.name);
                                next();
                            })

                    } else {
                        if (models[n].color)models[n].color = collorsList[models[n].color];
                        next();
                    }
                });

                // Подгружаем страну
                q.push(function (next) {

                    if (models[n].manufacturer && !countryList[models[n].manufacturer]) {

                        Countries.findById(models[n].manufacturer)
                            .then(function (item) {

                                if(item){
                                    countryList[models[n].manufacturer] = item.name;
                                    models[n].manufacturer = item.name;
                                }

                                next();
                            }, function (err) {
                                console.log('Error', models[n].manufacturer, item.name);
                                next();
                            })

                    } else {
                        if (models[n].manufacturer)models[n].manufacturer = countryList[models[n].manufacturer];
                        next();
                    }
                });

            })();

        }

        q.start(function (err) {
            //console.log("marksList", marksList);
            //console.log("collorsList", collorsList);
            //console.log('loadRelations3', models.length);
            if (err) {
                if (fail && typeof(fail) == 'function') fail(err);
            }
            else if (succeed && typeof(succeed) == 'function') succeed(models);

        });
    });
}

module.exports = router;