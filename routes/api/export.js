var express = require('express'),
    router = express.Router(),
    config = require('../../config/main'),
    passport = require('passport'),
    fs = require("fs"),
    queue = require('queue'),
    iconv = require('iconv-lite');

var moduleKey = 'export';

router.get('/' + moduleKey + '/:cat', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.cat;
    switch (cat) {
        case 'fridges' :
            modelName = 'Productsfridges';
            break;
        case 'washings' :
            modelName = 'Productswashings';
            break;
        case 'conditioners' :
            modelName = 'Productsconditioners';
            break;
        case 'hobs' :
            modelName = 'Productshobs';
            break;
        case 'ovens' :
            modelName = 'Productsovens';
            break;
        default :
            modelName = 'Productsfridges';
    }
    var model = require('../../models/' + modelName);

    var data = '';

    if (config.fields && config.fields[cat]) {

        var listColumns = config.fields[cat];

        var q = queue({concurrency: 1});

        q.push(function (next) {

            var list = model.find({})
                .then(function (list) {

                        for (var i = 0; i < listColumns.length; i++) {
                        data += listColumns[i] + ';';
                    }
                    data += "\n";

                    /*for (var n = 0; n < list.length; n++) {
                        var insideData = '';
                        for (var i = 0; i < listColumns.length; i++) {
                            if (listColumns[i]) {
                                if (list[n][listColumns[i]] && list[n][listColumns[i]] != 'undefined'){
                                    insideData += iconv.encode(list[n][listColumns[i]], 'utf-8' ) + ';';
                                }
                                else insideData += ';';
                            }
                        }
                        if (insideData)data += insideData + "\n";

                    }*/

                    next();
                    console.log(data);

                }, function (error) {
                    next();
                })

        });

        q.start(function (err) {

            fs.writeFile('./public/upload/csv/' + cat + '.csv', data, 'utf8', 0644, 'w+', function (err, file_handle) {
                if (err) {
                    res.send({status: 'error', error: err});
                } else {
                    res.send({status: 'ok', file: 'upload/csv/' + cat + '.csv'});
                }
            });

        });

    } else res.send({status: 'error', error: 'No list of fields for category'});

});

module.exports = router;