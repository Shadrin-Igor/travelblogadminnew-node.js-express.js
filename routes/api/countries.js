var express = require('express');
var router = express.Router();
var passport = require('passport');
var Countries = require('../../models/Countries');
var moduleKey = 'countries';

router.get('/' + moduleKey , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    Countries.find(function (err, data) {
        if (err)res.send({error: err});
            else res.send({data: data});
    })
});

module.exports = router;