let express = require('express'),
    router = express.Router(),
    moduleKey = 'editor-upload',
    passport = require('passport'),
    multiparty = require('multiparty'),
    im = require('imagemagick'),
    queue = require("queue"),
    fs = require('fs'),
    config = require('../../config/main'),
    FroalaEditor = require('wysiwyg-editor-node-sdk/lib/froalaEditor.js');
// passport.authenticate('jwt', {session: false}),
router.post('/' + moduleKey,  function (req, res, next) {

    // Store image.
    FroalaEditor.Image.upload(req, '../public/upload/editor/', function (err, data) {
        // Return data.
        if (err) {
            return res.send(JSON.stringify(err));
        }
        data.link = data.link.replace('../public/', config.baseUrl+'/');
        res.send(data);
    })

});

module.exports = router;