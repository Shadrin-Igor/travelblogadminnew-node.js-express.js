var express = require('express'),
    router = express.Router(),
    passport = require('passport');

var moduleKey = 'post_category';

router.get('/' + moduleKey , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var model = require('../../models/PostsCategory');
    var id = req.params.id;

    model.find(function (err, data) {
        if (err) res.send({status: 'error', error: err});
        else res.send({status: 'ok', data: data});

    })

});

router.get('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var model = require('../../models/PostsCategory');
    var id = req.params.id;

    model.findOne({id: id}, function (err, data) {
        if (err) res.send({status: 'error', error: err});
        else res.send({status: 'ok', data: data});

    })

});


router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var id = req.params.id;
    if (id) {
        var model = require('../../models/PostsCategory');
        model.remove({id: id}, function (err) {
            if (!err) res.send({status: 'deleted'});
            else res.send({status: false, error: err});
        })

    }
    else res.send({data: '', error: 'no id'});

});

router.post('/' + moduleKey , passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var data = req.body.data;

    if ( data) {
        var model = require('../../models/PostsCategory');
        var Model = new model();
        Model.id = Model._id;

        for (var field in data) {
            Model[field] = data[field];
        }

        console.log("add Model", Model);
        Model.save(function (err) {

            if (err) {
                console.log('add err', err, Model);
                res.send({error: err});
            }
            else {
                console.log('add ok', Model);
                res.send({data: Model.id, status: true});
            }

        });
    }
    else
        res.send({data: '', error: 'No data'});

});

router.put('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var id = req.params.id,
        data = req.body.data;

    if (data && data.id) {

        var model = require('../../models/PostsCategory');
        model.findById(data.id, function (error, Model) {
            if (data) {
                for (var field in data) {
                    Model[field] = data[field];
                }
                console.log("put Model", Model);
                Model.save()
                    .then(function (data) {
                        console.log('edit ok', Model);
                        res.send({data: true});

                    }, function (err) {
                        console.log('edit err', err, Model);
                        res.send({error: err});
                    });
            }
            else res.send({data: '', error: 'incorrect id'});
        });
    }
    else
        res.send({data: '', 'error': 'incorrect data'});

})

module.exports = router;