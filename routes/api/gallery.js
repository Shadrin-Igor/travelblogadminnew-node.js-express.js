var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    Galleries = require('../../models/Galleries').default,
    moduleKey = 'gallery',
    fs = require('fs'),
    queue = require("queue");

router.get('/' + moduleKey + '/:cat/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.cat;
    var id = req.params.id;
    if (cat && id) {
        Galleries.find({category: cat, item: id}, function (err, data) {
            if (err) res.send({error: err});
            else res.send({data: data});
        })
    }
    else res.send({error: 'get gallery error: have\'t parameters'});

});

router.delete('/' + moduleKey + '/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var id = req.params.id,
        q = queue();

    if (id) {
        Galleries.findOne({_id: id}, function (error, data) {
            if (!error) {

                console.log('data', data);
                // Delete original
                q.push(function(next){
                    fs.unlink("./public" + data.dir + data.image, function (err) {
                        console.info('delete image', "./public" + data.dir + data.image);
                        if (err)console.error('error', err);
                        next();
                    });
                });

                // Delete big
                q.push(function(next){
                    fs.unlink("./public" + data.dir + 'big_' + data.image, function (err) {
                        console.info('delete image', "./public" + data.dir + 'big_'+ data.image);
                        if (err)console.error('error', err);
                        next();
                    });
                });

                // Delete small
                q.push(function(next){
                    fs.unlink("./public" + data.dir + 'thumb_'+ data.image, function (err) {
                        console.info('delete image', "./public" + data.dir + 'thumb_'+ data.image);
                        if (err)console.error('error', err);
                        next();
                    });
                });

                q.start(function (err) {
                    Galleries.remove({_id: id}, function (err) {
                        if (!err)res.send({status: 'deleted'});
                        else res.send({status: false, error: err});
                    })
                });

            } else
                res.send({error: 'Delete gallery error: have\'t ID'});
        })
    }
    else res.send({error: 'Delete gallery error: have\'t parameters'});
});

module.exports = router;