var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    moduleKey = 'product',
    relationHelper = require('../../libs/relationHelper');

/**
 * Вызвращает список продукции
 */
router.get('/' + moduleKey + '/:category/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.category;
    var id = req.params.id;
    if (cat && id) {

        var modelName = relationHelper.getProductClass(req.params.category);
        var model = require('../../models/' + modelName);
        model.findOne({id: id}, function (err, data) {
            if (err)res.send({error: err});
            else res.send({data: data});
        })

    }
    else res.send({data: '', error: 'no category'});

});

/**
 * Удаление
 */

router.delete('/' + moduleKey + '/:category/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var cat = req.params.category;
    var id = req.params.id;
    if (cat && id) {
        console.log(cat, '-', id);

        var modelName = relationHelper.getProductClass(cat);
        var model = require('../../models/' + modelName);
        model.remove({id: id}, function (err) {
            if (!err)res.send({status: 'deleted'});
            else res.send({status: false, error: err});
        })

    }
    else res.send({data: '', error: 'no category'});

});

router.post('/' + moduleKey + '/:category/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var type = req.body.type;
    var data = req.body.data;

    if (type && data) {
        var modelName = relationHelper.getProductClass(cat);
        var model = require('../../models/' + modelName);
        Model.id = Model._id;

        for (var field in data) {
            Model[field] = data[field];
        }

        console.log("add Model", Model);
        Model.save(function (err) {

            if (err) {
                console.log('add err', err, Model);
                res.send({error: err});
            }
            else {
                console.log('add ok', Model);
                res.send({data: Model.id, status: true});
            }

        });
    }
    else
        res.send({data: '', error: 'No type'});

})

router.put('/' + moduleKey + '/:category/:id', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    var type = req.body.type,
        cat = req.params.cat,
        id = req.params.id,
        data = req.body.data;

    if (type && data && data.id) {
        var modelName = relationHelper.getProductClass(cat);
        var model = require('../../models/' + modelName);
        model.findById(data.id, function (error, Model) {
            if (data) {
                for (var field in data) {
                    Model[field] = data[field];
                }
                console.log("put Model", Model);
                Model.save()
                    .then(function (data) {
                        console.log('edit ok', Model);
                        res.send({data: true});

                    }, function (err) {
                        console.log('edit err', err, Model);
                        res.send({error: err});
                    });
            }
            else res.send({data: '', error: 'incorrect id'});
        });
    }
    else
        res.send({data: '', 'error': 'incorrect data'});

})

module.exports = router;