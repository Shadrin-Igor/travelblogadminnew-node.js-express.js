var express = require('express'),
    router = express.Router(),
    moduleKey = 'import',
    passport = require('passport'),
    fs = require('fs'),
    multiparty = require('multiparty'),
    queue = require("queue");

var Marks = require("../../models/Marks"),
    Collors = require("../../models/Collors"),
    Countries = require("../../models/Countries");

router.post('/' + moduleKey + '/:cat', passport.authenticate('jwt', {session: false}), function (req, res, next) {

    // создаем форму
    var form = new multiparty.Form();
    //здесь будет храниться путь с загружаемому файлу, его тип и размер
    var uploadFile = {uploadPath: '', type: '', size: 0};
    //максимальный размер файла
    var maxSize = 2 * 1024 * 1024 * 10; //20MB
    //поддерживаемые типы(в данном случае это картинки формата jpeg,jpg и png)
    var supportMimeTypes = ['application/vnd.ms-excel'];
    //массив с ошибками произошедшими в ходе загрузки файла
    var errors = [];

    //если произошла ошибка
    form.on('error', function (err) {
        if (fs.existsSync(uploadFile.path)) {
            //если загружаемый файл существует удаляем его
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function () {

        //если нет ошибок и все хорошо
        if (errors.length == 0) {

            var columns = {};
            columns["fridges"] = ['mark', 'model', 'manufacturer', 'price', 'volume', 'color', 'control', 'energy_consumption', 'energy_class', 'location_freezer', 'inverter', 'number_cameras', 'number_doors', 'zone_freshness', 'type_cooling', 'dimensions', 'climatic_class', 'overweighing_door', 'weight', 'holidays_mode', 'options', 'display', 'freezing_capacity', 'refrigerant', 'noise_level', 'additional_features'];
            columns["washings"] = ['mark', 'model', 'manufacturer', 'price', 'download_type', 'maximum_load', 'drying', 'control', 'display', 'direct_drive', 'dimensions', 'weight', 'colour', 'energy_class', 'number_revolutions', 'protection_children', 'control_imbalance', 'foam_control', 'number_programs', 'programs', 'delay_timer', 'material', 'loading_door', 'noise', 'loading_linen', 'additional_features', 'annual_consumption', 'maximum_temperature', 'door'];
            columns["conditioners"] = ['mark', 'model', 'price', 'color', 'manufacturer', 'serviced_area', 'energy_class', 'modes', 'cooling_capacity', 'heating_power', 'power_heating', 'power_cooling', 'additional_modes', 'drying_mode', 'd_u', 'timer', 'noise_level', 'phase', 'air_filters', 'fan_control', 'minimum_temperature', 'additional_information', 'other_functions', 'dimensions', 'weight_indoor', 'dimensions_outdoor', 'weight_external', 'freon'];
            columns["hobs"] = ['mark', 'model', 'manufacturer', 'price', 'type', 'dimensions', 'dimensions_embedding', 'material', 'total_hotplates', 'gas_burners', 'type_consolidation', 'location_control', 'security_system', 'electric_ignition', 'gas_control', 'iron_grilles', 'color', 'display_type'];
            columns["ovens"] = ['mark', 'model', 'manufacturer', 'price', 'type', 'controls', 'scope', 'dimensions', 'modes_operation', 'grill', 'convection', 'niche_size', 'rotisserie', 'oven_door', 'lighting', 'equipment', 'energy_class', 'maximum_temperature', 'sleep_timer', 'indication', 'color', 'display_type'];

            var cat = req.params.cat;
            if (!columns[req.params.cat]) {
                res.send({status: 'error', errors: 'Incorrect caetgory'});
            }

            console.log("cat", req.params.cat);

            var model = require("../../models/Products" + req.params.cat);

            fs.readFile(uploadFile.path, 'utf8', function (err, contents) {
                if (err)return res.send({status: 'error', errors: errors});

                var q = queue({concurrency: 1});
                var listModels = [];

                var lines = contents.toString().split('\n');
                for (var i = 1; i < lines.length; i++) {
                    listModels[i] = new model();
                    listModels[i].id = listModels[i]._id;
                    var data = [];
                    data = lines[i].split(';');

                    for (var n = 0; n < data.length; n++) {
                        (function () {
                            var num = n;
                            var lineData = data[num];
                            var i2 = i;
                            q.push(function (cb) {

                                var column = columns[cat][num];
                                switch (column) {
                                    case 'mark' :
                                        if (lineData) {
                                            Marks.findOne({name: lineData})
                                                .then(function (item) {

                                                    if (item) {
                                                        listModels[i2][column] = item["id"];
                                                        listModels[i2]['mark-name'] = item["name"];
                                                        cb();
                                                    }
                                                    else {
                                                        console.log('Error not find anything');

                                                        var newModel = new Marks();
                                                        newModel.name = lineData;
                                                        newModel.id = newModel._id;

                                                        newModel.save()
                                                            .then(function (item) {
                                                                console.log('Add new mark', column, item);
                                                                listModels[i2][column] = item["id"];
                                                                listModels[i2]['mark-name'] = item["name"];
                                                                cb();
                                                            }, function (err) {
                                                                console.log('Add mark error', err);
                                                                listModels[i2][column] = '';
                                                                cb();
                                                            })
                                                    }


                                                }, function (error) {
                                                    console.log('error find mark', error);
                                                    listModels[i2][column] = '';
                                                    cb();
                                                });


                                        } else cb();
                                        break;
                                    case 'color' :
                                        if (lineData) {
                                            Collors.findOne({name: lineData}, function (err, item) {
                                                if (err) return err;
                                                if (item) {
                                                    listModels[i2][column] = item["id"];
                                                    cb();
                                                }
                                                else {
                                                    var newModel = new Collors();
                                                    newModel.name = lineData;
                                                    newModel.id = newModel._id;

                                                    // @TODO надо предалать чтобы проверял существование
                                                    newModel.save()
                                                        .then(function (item) {
                                                            console.log('Add new color', column, lineData);
                                                            listModels[i2][column] = item["id"];
                                                            listModels[i2]['color-name'] = item["name"];
                                                            cb();
                                                        }, function (err) {
                                                            console.log('Add color error', err);
                                                            cb();
                                                        })

                                                }
                                            });
                                        } else cb();
                                        break;
                                    case 'manufacturer' :
                                        if (lineData) {
                                            Countries.where({name: lineData}).findOne(function (err, item) {
                                                if (err) return err;
                                                if (item) {
                                                    listModels[i2][column] = item["id"];
                                                    cb();
                                                }
                                                else {
                                                    var newModel = new Countries();
                                                    newModel.name = lineData;
                                                    newModel.id = newModel._id;

                                                    // @TODO надо предалать чтобы проверял существование
                                                    newModel.save()
                                                        .then(function (item) {
                                                            console.log('Add new country', column, lineData);
                                                            listModels[i2][column] = item["id"];
                                                            listModels[i2]['country-name'] = item["name"];
                                                            cb();
                                                        }, function (err) {
                                                            console.log('Add country error', err);
                                                            cb();
                                                        })

                                                }
                                            });
                                        } else cb();
                                        break;
                                    default :
                                        listModels[i2][column] = lineData;
                                        cb();
                                        break;
                                }

                            });
                        })();

                    }
                }

                q.start(function (err) {

                    model.remove({})
                        .then(function () {
                            listModels.map(function (item, key) {

                                // Выставляем SLUG
                                if(item['mark'] && item['model'] ){
                                    item.slug = item['mark-name']+'-'+item.model;
                                    item.slug = item.slug.toLowerCase();
                                    item.slug = item.slug.replace(/[А-яа-я\!@#\$&\*\^]/gi, '');
                                    item.slug = item.slug.replace(/[|/\\ ]/gi, '-');
                                }

                                item.save()
                                    .then(function (item) {
                                        //console.log('save product', item);
                                    }, function (error) {
                                        //console.log('save product error', item, error);
                                    })
                            });
                            fs.unlinkSync(uploadFile.path);

                            loadRelations(listModels)
                                .then(function (models) {
                                    res.send({status: 'ok', text: 'Success', items: models});
                                }, function (err) {
                                    console.log('loadRelations error', err);
                                    res.send({status: 'error', error: err});
                                })

                        })
                });

            });

        }
        else {
            if (fs.existsSync(uploadFile.path)) {
                //если загружаемый файл существует удаляем его
                fs.unlinkSync(uploadFile.path);
            }
            //сообщаем что все плохо и какие произошли ошибки
            res.send({status: 'bad', errors: errors});
        }
    });

    // при поступление файла
    form.on('part', function (part) {

        //читаем его размер в байтах
        uploadFile.size = part.byteCount;
        //читаем его тип
        uploadFile.type = part.headers['content-type'];
        //путь для сохранения файла
        uploadFile.path = 'public/upload/csv/' + part.filename;

        //проверяем размер файла, он не должен быть больше максимального размера
        if (uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }

        //проверяем является ли тип поддерживаемым
        if (supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        //если нет ошибок то создаем поток для записи файла
        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            //пропускаем
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            part.resume();
        }
    });

    // парсим форму
    form.parse(req);

});

function loadRelations(models) {
    var marksList = {};
    var collorsList = {};
    var countryList = {};

    return new Promise(function (succeed, fail) {
        var q = queue({concurrency: 1});

        for (var i = 0; i < models.length; i++) {

            (function () {

                var n = i;

                if (models[n]) {

                    // Подгружаем марки
                    q.push(function (next) {

                        if (models[n].mark && !marksList[models[n].mark]) {

                            Marks.findById(models[n].mark)
                                .then(function (item) {
                                    if (item) {
                                        console.log('loadRelations mark', models[n].mark, item.name);
                                        marksList[models[n].mark] = item.name;
                                        models[n].mark = item.name;
                                    }

                                    next();
                                }, function (err) {
                                    console.log('Error', models[n].mark, item.name);
                                    next();
                                })

                        } else {
                            if (models[n].mark && marksList[models[n].mark])models[n].mark = marksList[models[n].mark];
                            next();
                        }

                    });

                    // Подгружаем цвета
                    q.push(function (next) {

                        if (models[n].color && !collorsList[models[n].color]) {

                            Collors.findById(models[n].color)
                                .then(function (item) {

                                    if (item) {
                                        collorsList[models[n].color] = item.name;
                                        models[n].color = item.name;

                                    }

                                    next();
                                }, function (err) {
                                    console.log('Error', models[n].color, item.name);
                                    next();
                                })

                        } else {
                            if (models[n].color)models[n].color = collorsList[models[n].color];
                            next();
                        }
                    });

                    // Подгружаем страну
                    q.push(function (next) {

                        if (models[n].manufacturer && !countryList[models[n].manufacturer]) {

                            Countries.findById(models[n].manufacturer)
                                .then(function (item) {

                                    if (item) {
                                        countryList[models[n].manufacturer] = item.name;
                                        models[n].manufacturer = item.name;
                                    }

                                    next();
                                }, function (err) {
                                    console.log('Error', models[n].manufacturer, item.name);
                                    next();
                                })

                        } else {
                            if (models[n].manufacturer)models[n].manufacturer = countryList[models[n].manufacturer];
                            next();
                        }
                    });

                }

            })();

        }

        q.start(function (err) {
            //console.log("marksList", marksList);
            //console.log("collorsList", collorsList);
            //console.log('loadRelations3', models.length);
            if (err) {
                if (fail && typeof(fail) == 'function') fail(err);
            }
            else if (succeed && typeof(succeed) == 'function') succeed(models);

        });
    });
}

module.exports = router;