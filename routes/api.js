let express = require("express"),
    router = express.Router(),
    passport = require("passport"),
    queue = require("queue"),
    moment = require("moment"),
    jwt = require('jwt-simple');

let postApi = require("./api/post.js"),
    countries = require("./api/countries.js"),
    postCategoryApi = require("./api/post_category.js"),
    admin = require("./api/admin.js"),
    upload = require("./api/upload.js"),
    gallery = require("./api/gallery.js"),
    exportApi = require('./api/export.js'),
    editorUpload = require('./api/editor-upload.js');

let adminModel = require("../models/Admins"),
    AccessToken = require("../models/AccessToken");

router.get('/', (req, res, next) => {
    res.json({'error': 'Invalid URL'});
});

router.get('/unauthorized', (req, res, next) => {
    res.json({'error': 'unauthorized'});
});

router.post('/auth', (req, res, next) => {
    let email = req.body.email || '';
    let password = req.body.password || '';
    let error = '';
    if (email && password) {
        adminModel.findOne((err, adminDB) => {

            if (err || !adminDB || adminDB.length === 0) {
                if (!adminDB || adminDB.length === 0) err = 'incorrect email or password';
                else res.json({'auth': 'fail', 'error': err});
            }
            else {
                let q = queue({concurrency: 1});
                let newAccessToken = '';
                if (adminDB.verifyPassword(password)) {

                    let ip = req.connection.remoteAddress && req.connection.remoteAddress != '::1' ? req.connection.remoteAddress : '127.0.0.1';

                    q.push((cb) => {
                        AccessToken.findOne({"adminId": adminDB.id})
                            .then((data) => {

                                if (!err && data) {
                                    if (ip == data.ip) {
                                        AccessToken.update({_id: data._id}, {'date': moment().format()}, (err, result) => {

                                            if (result && result.ok) newAccessToken = data;
                                            else error = "auth db error";

                                            cb();
                                        });
                                    }
                                    else {
                                        // Удаляем еслит срхраненный IP не соответствует IP пользователя
                                        AccessToken.remove({_id: data._id}).then((err) => {
                                            cb();
                                        })
                                    }
                                }
                                else {
                                    if (err) error = err;
                                    cb();
                                }
                            }, (err) => {
                                error = err;
                                cb();
                            });

                    });

                    q.push((cb) => {
                        if (!error && !newAccessToken) {
                            newAccessToken = new AccessToken({
                                'adminId': parseInt(adminDB.id),
                                'ip': ip,
                                'token': jwt.encode({id: adminDB.id}, "devdacticIsAwesome")
                            });

                            newAccessToken.save()
                                .then((newToken) => {
                                    newAccessToken = newToken;
                                    cb();
                                }, (err) => {
                                    console.log('error', err.message);
                                    error = err.message;
                                    cb();
                                });
                        }
                        else cb();
                    });
                }
                else error = 'incorrect email or password';

                q.start((err) => {
                    if (!error && !newAccessToken.token) error = "auth token error";
                    if (!error && newAccessToken) res.json({
                        'success': 'true',
                        'token': newAccessToken.token,
                        'user': {'id': adminDB.id, 'email': adminDB.email, 'role': adminDB.role}
                    });
                    else {
                        console.log(newAccessToken);
                        res.json({'auth': 'fail', 'error': error});
                    }
                });
                /*            if( adminDB.verifyPassword( password ) ){
                 AccessToken.find( {"userId":adminDB.id}, ( error, data ) => {
                 if( data ){
                 data.update
                 }
                 else {
                 let newAccessToken = new AccessToken( {'userId':adminDB.id } );
                 newAccessToken.save()
                 .then((data) => {
                 res.json({'auth' : 'success', 'user':{'id':adminDB.id,'email':adminDB.email,'role':adminDB.role,'token':data.token}});
                 });
                 }

                 });*/
            }
        })

    } else error = 'not have required field';

    if (error) res.json({'auth': 'fail', 'error': error});

});

module.exports = []
    .concat(router)
    .concat(postApi)
    .concat(postCategoryApi)
    .concat(upload)
    .concat(countries)
    .concat(gallery)
    .concat(exportApi)
    .concat(admin)
    .concat(editorUpload);