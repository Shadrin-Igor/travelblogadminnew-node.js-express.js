let express = require("express"),
    router = express.Router(),
    moment = require('moment'),
    postsModel = require("../../models/Posts");

router.get('/', (req, res, next) => {
    postsModel.find(function (err, data) {
        postsModel.loadRelations(data)
            .then((posts) => {
                res.render("site/index", {listProducts: posts, moment});
            })
            .catch((error) => {
                res.send({status: 'fail', error});
            })

    });
});

module.exports = router;