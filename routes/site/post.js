let express = require("express"),
    router = express.Router(),
    moment = require("moment"),
    queue = require("queue"),
    postsModel = require("../../models/Posts");

router.get('/post/:id.html', (req, res, next) => {
    let q = queue({concurrency: 1}),
        post = [],
        otherPosts = [],
        error = '';

    q.push((next) => {
        postsModel.find({slug: req.params.id}, (err, data) => {
            postsModel.loadRelations(data)
                .then((dataPosts) => {
                    if (!dataPosts[0] || !dataPosts[0].id) {
                        error = 'Incorrect ID';
                    } else {
                        post = dataPosts[0];
                    }
                    next();
                })
                .catch((findError) => {
                    error = findError;
                    next();
                });
        });
    });

    q.push((next) => {
        if (post) {
            postsModel.find({id: {$ne: post.id}})
                .limit(16)
                .then((otherData) => {
                    postsModel.loadRelations(otherData)
                        .then((dateWithRelation) => {
                            otherPosts = dateWithRelation;
                            next();
                        })
                        .catch((errorRelation)=>{
                            error = errorRelation;
                            next();
                        });

                })
                .catch((errorOther) => {
                    error = errorOther;
                    next();
                });
        } else {
            next();
        }

    });

    q.start((err = '') => {
        if (err) {
            res.send({status: 'fail', error});
        } else {
            res.render("site/post", {post, moment, otherPosts});
        }

    });
});

module.exports = router;