let express = require("express"),
    router = express.Router(),
    index = require("./site/index"),
    post = require("./site/post");

module.exports = []
    .concat(router)
    .concat(index)
    .concat(post);