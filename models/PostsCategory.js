var mongoose = require('mongoose');

module.exports = mongoose.model('posts_categories',
    new mongoose.Schema({
        id: { type: String, required: true, unique: true },
        name: { type: String, required: true },
        description: { type: String },
        small: { type: String },
        date: {type: Date, default: Date.now},
        title: {type: String},
        category: { type: String},
        metatitle: {type: String},
        metadescription: {type: String},
        metakeys: {type: String}
    })
);