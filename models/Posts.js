let mongoose = require('mongoose'),
    queue = require('queue'),
    _ = require('lodash'),
    PostsCategory = require('./PostsCategory'),
    Galleries = require('./Galleries');

let schema = new mongoose.Schema({
    id: {type: String, required: true, unique: true},
    name: {type: String, required: true},
    slug: {type: String},
    description: {type: String},
    image: {
        file: {type: String},
        dir: {type: String}
    },
    small: {type: String},
    date: {type: Date, default: Date.now},
    title: {type: String},
    category: {type: String, required: true},
    metatitle: {type: String},
    metadescription: {type: String},
    metakeys: {type: String}
});

schema.pre('save', function (next) {
    if (!this.slug && this.name) {
        this.slug = this.name.toLowerCase().replace(/[ =.]+/g, "-");
        this.slug = this.slug.replace(/[^a-z0-9-_а-я]*/g, "");
        this.slug = this.slug.replace(/-{2,}/g, "-");

        let space = '';
        let transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
            'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya'
        };

        let translateString = '';
        for (let i = 0; i < this.slug.length; i++) {
            if (!transl[this.slug[i]]) {
                translateString += this.slug[i];
            } else {
                translateString += transl[this.slug[i]];
            }
        }
        this.slug = translateString;
    }
    next();
});

let Post = mongoose.model('posts', schema);

Post.loadRelations = (modelsIn) => {
    let categoryList = {},
        models = _.cloneDeep(modelsIn),
        result = [];

    return new Promise((succeed, fail) => {
        let q = queue({concurrency: 1});
        let test = {};

        for (let i = 0; i < models.length; i++) {
            let modelItem = {};
            for (let field in schema.obj) {
                modelItem[field] = models[i][field];
            }

            // Подгружаем категорию
            q.push((next) => {
                if (models[i].category) {
                    if (!categoryList[models[i].category]) {
                        PostsCategory.findById(models[i].category)
                            .then((item) => {
                                if (item) {
                                    let categoryKey = models[i].category;
                                    categoryList[categoryKey] = {name: item.name, id: item.id};
                                    modelItem['category'] = null;
                                    modelItem['category'] = {name: item.name, id: item.id};
                                    result.push(modelItem);
                                }
                                next();
                            })
                            .catch((err) => {
                                console.error('Error', err);
                                next();
                            })

                    } else {
                        if (modelItem['category']) {
                            modelItem['category'] = categoryList[modelItem['category']];
                            result.push(modelItem);
                        }
                        next();
                    }
                } else {
                    modelItem['category'] = {};
                    result.push(modelItem);
                    next();
                }
            });
        }

        q.start((err = '') => {
            if (err) {
                if (fail && typeof(fail) === 'function') {
                    fail(err);
                }
            }
            else if (succeed && typeof(succeed) === 'function') {
                succeed(result);
            }

        });
    });
};

module.exports = Post;